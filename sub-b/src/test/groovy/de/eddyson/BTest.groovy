package de.eddyson

import spock.lang.Specification

class BTest extends Specification {

  def "Test B"() {
    expect:
      B.getName() == "B"
  }
}
