package de.eddyson

import spock.lang.Specification

class ATest extends Specification {

  def "Test A"() {
    expect:
      A.getName() == "A"
  }
}
