# Demo project to demonstrate gradle caching is not used in Intellij 2020.3

See issue: [IDEA-234052](https://youtrack.jetbrains.com/issue/IDEA-234052)

## How to replicate

1. Import project into Intellij
2. Execute `./gradlew test` at the command line.
3. Execute the test task via the Gradle Tool Window (test tasks are executed again, despite the cache should be used).
4. Execute `./gradlew test` at the command line again to verify the cache is used.

## Notes
* Not sure if the subprojects are actually needed to replicate this, but it is similar to our real setup.